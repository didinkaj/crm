<?php

namespace CRM;

use Illuminate\Database\Eloquent\Model;

class Contacts extends Model
{
    //
     protected $fillable = [
    			'names','idno','staffno','cellno','physicaladdress','photo'
    			];
}

