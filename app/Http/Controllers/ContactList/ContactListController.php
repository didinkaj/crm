<?php

namespace CRM\Http\Controllers\ContactList;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Input;
use Auth;
use CRM\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Yajra\Datatables\Datatables;
use Yajra\DataTables\Html\Builder;
use Session;
use CRM\Contacts;
use CRM\User;

class ContactListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $allcontacts =  Contacts::latest() -> orderBy('created_at', 'desc') -> get();
        return view('/dashboard/home',compact('allcontacts'));
    }
    /**
        return any data using yajar ajax datatables users list
    */
    public function anyData()
    {
        return Datatables::of(User::query())->make(true);
    }
    /*
        contact list
    */
    public function contactList()
    {
    if (request()->ajax()) {
        return DataTables::of(Contacts::query())->toJson();
    }

    $html = $builder->columns([
                ['data' => 'id', 'name' => 'id', 'title' => 'Id'],
                ['data' => 'name', 'name' => 'name', 'title' => 'Name'],
                ['data' => 'idno', 'name' => 'idno', 'title' => 'idno'],
                ['data' => 'created_at', 'name' => 'created_at', 'title' => 'Created At'],
                ['data' => 'updated_at', 'name' => 'updated_at', 'title' => 'Updated At'],
            ]);

    return view('dashboard.home', compact('html'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'cname' => 'required|max:255|string',
            'cidno' => 'required|Numeric',
            'staffno' => 'required|string',
            'cellno' => 'required | Numeric | unique:contacts,cellno',
            'paddress' => 'required|string',
            'pphoto' => '',
        ]);

        if ($validator->fails()) {
            session::flash('error', 'Customer Creation failed, please try again');
            return redirect('/dash')
                        ->withErrors($validator)
                        ->withInput();
        }else{
           
            $filename = $request->input(['cellno']).'_'.$request->input(['cidno']).'.'.$request->pphoto->extension();
            $path = $request->file('pphoto')->storeAS('profilePhotos',$filename,'public');
            if($request->hasFile('pphoto') && $request->file('pphoto')->isValid()){
                $path = $path;                
           }else{
            $path="";            
           }
            
            $save =Contacts::create([
                        'names' => $request->input(['cname']),
                        'idno' => $request->input(['cidno']),
                        'staffno' => $request->input(['staffno']),
                        'cellno' => $request->input(['cellno']),
                        'physicaladdress' => $request->input(['paddress']),
                        'photo' => $path
                        ]);
            // redirect
            if($save){
            session::flash('success', 'Customer Created Successfully ');
            return redirect('/dash');
            }else{
            session::flash('error', 'Error Customer not saved, try again!');
            return redirect('/dash');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $delete = Contacts::destroy($id);
        if($delete){
            session::flash('success', 'Successfully deleted contact!');
            return redirect('/dash');
        }else{
            session::flash('error', 'Deletion failed, please try again');
            return redirect('/dash');
        }
    }
}
