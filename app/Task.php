<?php

namespace CRM;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    //
    protected $fillable = [
    			'body','user_id','email','department','access'
    			];
    
}
