@extends('layouts.main')

@section('title', 'Contact List ')

@section('styles')

@parent
<!-- FooTable -->

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
<link href="{!! asset('css/plugins/footable/footable.core.css') !!}" rel="stylesheet">
<link href="{!! asset('css/plugins/select2/select2.min.css') !!}" rel="stylesheet">
@endsection

@section('content')
<div class="row wrapper white-bg border-bottom page-heading">
    <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
        <h1 style="color: green;"> Customers Contact List </h1>
        <ol class="breadcrumb">
            <li>
                <a href="/tasks">Contact list</a>
            </li>
            <li class="active">
                <a>Customer Details</a>
            </li>

        </ol>

    </div>
    <div class="col-xs-10 col-sm-10 col-md-10 col-lg-2">
        <h1></h1>
        <a class="minimalize-styl-2 btn btn-success " id="task" data-toggle="modal" data-target="#myModal" href="#"> <i class="fa fa-user-plus"></i>
        <br/>
        Add Customer</a>
    </div>
</div>


<div class="wrapper wrapper-content animated fadeInRight" id="app">

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    @isset($allcontacts)
                    <h5>Customer List </h5>
                    @endisset

                    

                    <div class="ibox-tools">
                        <a class="collapse-link"> <i class="fa fa-chevron-up"></i> </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#"> <i class="fa fa-wrench"></i> </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li>
                                <a href="#">Config option 1</a>
                            </li>
                            <li>
                                <a href="#">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link"> <i class="fa fa-times"></i> </a>
                    </div>
                </div>

                <div class="ibox-content">
                    @if (Session::has('success'))
                    <span class="alert alert-success alert-dismissable col-md-12"> <strong>{{ Session::get('success') }}</strong>
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">
                            ×
                        </button></span>
                    @endif
                    @if (Session::has('error'))
                    <span class="alert alert-danger alert-dismissable col-md-12"> <strong>{{ Session::get('error') }}</strong>
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">
                            ×
                        </button></span>
                    @endif

                    @isset($allcontacts)
                    <div class="input-group m-b placeholodercolor">
                        <input type="text" class="form-control input-sm m-b-xs " id="filter" placeholder="Search in table">
                        <span class="input-group-addon btn-success"><i class="fa fa-search " style="color: #0E9AEF;"></i></span>
                    </div>

                    <table class="footable table table-stripped table-bordered " data-page-size="8" data-filter=#filter>

                        <thead>
                            <tr>
                                <th>Names</th>
                                <th>ID No</th>
                                <th data-hide="phone,tablet">Staff No</th>
                                <th data-hide="phone,tablet">Cell No</th>
                                <th data-hide="phone,tablet"> Physical Address</th>
                                <th>Passport Photo</th>
                                <th data-hide="phone,tablet">Created </th>
                                <th data-hide="phone,tablet">Action </th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($allcontacts as $contactdetails)
                            <tr class="gradeX">
                                <td class="center">{{$contactdetails->names}}</td>
                                <td>{{ucfirst($contactdetails->idno)}} </td>
                                <td>{{ucfirst($contactdetails->staffno)}}</td>
                                <td class="center">{{$contactdetails->cellno}}</td>
                                <td class="center">{{$contactdetails->physicaladdress}}</td>
                                <td class="center"><img src="{{ asset('storage/'. $contactdetails->photo) }}" width="40px" /></td>
                                <td class="center" valign="center">{{$contactdetails->created_at->diffForHumans()}}</td>                                
                                <td class="center">

                                <a href="#" style="color:red; background-color:;" title="Delete Contact" data-toggle="modal" data-target="#myModal{{$contactdetails->id}}" class="btn btn-white btn-bitbucket">
                                    <i class="fa fa-remove"></i>
                                </a> 

                                    <!-- modal window for delete -->
                                    <div class="modal inmodal" id="myModal{{$contactdetails->id}}" tabindex="-1" role="dialog" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content animated bounceInDown">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">
                                                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                                                    </button>
                                                    <i class="fa fa-warning modal-icon"></i>
                                                    <h4 class="modal-title">Delete Contact</h4>
                                                    <h4 class="font-bold" style="color: red; ">Are you sure you want to delete</h4>
                                                    <h3>{{$contactdetails->names}}</h3>

                                                     <form method="POST" action="/deletecontact/{{$contactdetails->id}}" class="pull-right">
                                                        {!! csrf_field() !!}
                                                        {!! method_field('delete') !!}
                                                        <input  name="Delete" value="Delete" type="submit"   class="btn" style="background-color: red;color: #ffffff;"/>
                                                            

                                                    </form>&nbsp;&nbsp;                                                   

                                             
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end of modal window -->

                                 </td>
                            </tr>


                            @endforeach

                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="11"><ul class="pagination pull-right"></ul></td>
                            </tr>
                        </tfoot>
                    </table>
                    @endisset


                    <!-- test for yajar datatables -->
                    @isset($html)
                    {!! $html->table() !!}
                    @endisset

                    <table class="footable table table-bordered table-stripped" id="users-table1">
                        <thead>
                            <tr>
                                <th>Id No</th>
                                <th>name</th>
                                <th data-hide="phone,tablet">email</th>
                                <th data-hide="phone,tablet">Created </th>
                                <th data-hide="phone,tablet">updated </th>
                            </tr>
                        </thead>

                    </table>

                   
                </div>
            </div>
        </div>
    </div>

</div>

<!-- modal window -->
<div class="modal inmodal in" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInDown">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                <i class="fa fa-user-plus modal-icon" style="color: #1a7bb9;"></i>
                <h4 class="modal-title">Add New Customer</h4>
                <small class="font-bold" style="color: green;">Customer Registration form</small>
            </div>
            <form method="post" action="/createcustomer" enctype="multipart/form-data">
                <div class="modal-body col-md-12">
                <div class="col-md-6">

                    {{ csrf_field() }}
                    <div class="form-group {{ $errors->has('cname') ? ' has-error' : '' }}">
                        <label for="cname">Names</label>
                        <input type="text" placeholder="" name="cname" value="{{old('cname')}}" class="form-control">
                        @if ($errors->has('cname'))
                        <span class="help-block"> <strong>{{ $errors->first('cname') }}</strong> </span>
                        @endif
                    </div>

                    <div class="form-group {{ $errors->has('cidno') ? ' has-error' : '' }}">
                        <label for="cidno">ID No</label>
                        <input type="text" placeholder="" name="cidno" value="{{old('cidno')}}" class="form-control">
                        @if ($errors->has('cidno'))
                        <span class="help-block"> <strong>{{ $errors->first('cidno') }}</strong> </span>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('staffno') ? ' has-error' : '' }}">
                        <label for="staffno">Staff No</label>
                        <input type="text" placeholder="" name="staffno" value="{{old('staffno')}}" class="form-control">
                        @if ($errors->has('staffno'))
                        <span class="help-block"> <strong>{{ $errors->first('staffno') }}</strong> </span>
                        @endif
                    </div>
                    </div>
                    <div class="col-md-6">
                    <div class="form-group {{ $errors->has('cellno') ? ' has-error' : '' }}">
                        <label for="cellno">Cell No</label>
                        <input type="text" placeholder="" name="cellno" value="{{old('cellno')}}" class="form-control">
                        @if ($errors->has('cellno'))
                        <span class="help-block"> <strong>{{ $errors->first('cellno') }}</strong> </span>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('paddress') ? ' has-error' : '' }}">
                        <label for="paddress">Physical Address</label>
                        <input type="text" placeholder="" name="paddress" value="{{old('paddress')}}" class="form-control">
                        @if ($errors->has('paddress'))
                        <span class="help-block"> <strong>{{ $errors->first('paddress') }}</strong> </span>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('pphoto') ? ' has-error' : '' }}">
                        <label for="pphoto">Passport Photo</label>
                        <input type="file" placeholder="" name="pphoto" value="{{old('pphoto')}}" class="form-control">
                        @if ($errors->has('pphoto'))
                        <span class="help-block"> <strong>{{ $errors->first('pphoto') }}</strong> </span>
                        @endif
                    </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">
                        Cancel
                    </button>
                    <input type="submit" class="btn btn-primary " style="background-color:green;"value="Save"/>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end of modal window -->

@endsection


@section('scripts')

  
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
<!-- yajar datatables scrip -->
<script>
$(function() {
    $('#users-table1').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('datatablesData') !!}',
        columns: [
            { data: 'id', name: 'id' },
            { data: 'names', name: 'names' },
            { data: 'idno', name: 'idno' },
            { data: 'created_at', name: 'created_at' },
            { data: 'updated_at', name: 'updated_at' }
        ]
    });
});
</script>

<!--modol erro -->
@if(!empty(Session::get('error')) )
<script>
    $(document).ready(function() {

        $('#myModal').modal('show');

    }); 
</script>
@endif

<script>
        $(document).ready(function() {

            $('.footable').footable();
            $('.footable2').footable();

        });

        $('.modal-header').on('click', '.delete', function() {
            $.ajax({
                type: 'DELETE',
                url: 'deletecontact/' + id,
                data: {
                    '_token': $('input[name=_token]').val(),
                },
                success: function(data) {
                    toastr.success('Successfully deleted Post!', 'Success Alert', {timeOut: 5000});
                    $('.item' + data['id']).remove();
                }
            });
        });

</script>


@parent
<!-- FooTable -->
<script src="{{ asset('js/plugins/footable/footable.all.min.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('js/plugins/select2/select2.full.min.js') }}"></script>
<!-- Page-Level Scripts -->

@endsection
