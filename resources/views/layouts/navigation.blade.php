<nav class="navbar-default navbar-static-side" role="navigation">
	<div class="sidebar-collapse">
		<ul class="nav metismenu" id="side-menu">
			<li class="nav-header">
				<div class="dropdown profile-element">
					<span> </span>
					<a data-toggle="dropdown" class="dropdown-toggle" href="#"> <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold"><h3>CRM</h3></strong> </span> 
						<span class="text-muted text-xs block">@if(!Auth::guest())<?php $pieces = explode(" ",Auth::user()->name); ?>{{ ucfirst($pieces[0]) }}@endif <b class="caret"></b></span> </span> </a>
					<ul class="dropdown-menu animated fadeInRight m-t-xs">
						<li>
							<a href="{{ route('logout') }}"
							onclick="event.preventDefault();
							document.getElementById('logout-form').submit();"> <i class="fa fa-sign-out"></i> Log out </a>
							<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
								{{ csrf_field() }}
							</form>
						</li>
					</ul>
				</div>
				<div class="logo-element">
					TS
				</div>
			</li>
			<li class="{{ isActiveRoute('dashboard') }} {{ isActiveRoute('home') }}" >
				<a href="{{ url('/dash') }}" class="text-center"><i class="fa fa-address-book fa-2x"></i>
				<br/>
				<span class="nav-label">Contacts List</span></a>
			</li>
			<li class="{{ isActiveRoute('projects') }}">
				<a href="{{url('#')}}" class="text-center"><i class="fa fa-th-large fa-2x"></i>
				<br/>
				<span class="nav-label">Ticketing Log</span> </a>
			</li>
			
		

		</ul>

	</div>
</nav>
